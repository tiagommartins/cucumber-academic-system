# Testando portal do aluno Cucumber+Capybara

    git clone https://tiagommartins@bitbucket.org/tiagommartins/cucumber-academic-system.git
    cd cucumber-academic-system
    bundle install
    bundle exec cucumber

Este projeto está configurado para utilizar `:selenium` como driver padrão, mas pode ser alterado em `/features/support/env.rb`.