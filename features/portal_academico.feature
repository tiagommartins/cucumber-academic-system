Feature: Portal Academico

Scenario: Login no Portal Academico

Given I am on "https://academicos.unilasalle.edu.br/portal.php"
Given Switch to frame "corpo"
Given fill form entry "l_user" with "200710485"
Given fill form entry "l_pwd" with "123456"
When click on "Entrar" Button
Then I should see user identify "Boa noite, TIAGO MARTINS MARTINS martins.tiago.m@gmail.com"

Scenario: Falha na autenticacao no Portal Academico

Given I am on "https://academicos.unilasalle.edu.br/portal.php"
Given Switch to frame "corpo"
Given fill form entry "l_user" with "200710485"
Given fill form entry "l_pwd" with "098765"
When click on "Entrar" Button
Given Switch to frame "corpo"
Then I should see the text "Matrícula ou senha incorreta!!"

Scenario: Visualizacao de notas

Given I am on "https://academicos.unilasalle.edu.br/portal.php"
Given Switch to frame "corpo"
Given fill form entry "l_user" with "200710485"
Given fill form entry "l_pwd" with "123456"
When click on "Entrar" Button
Given Switch to frame "corpo"
Given Switch to frame "conteudoPrincipal"
Then I should see the following table rows:
  | 2344 - QUALIDADE E TESTE DE SOFTWARE   | 9,0 |
  | 1983 - EMPREENDEDORISMO E CRIATIVIDADE | 9,1 |

Scenario: Visualizacao de docs

Given I am on "https://academicos.unilasalle.edu.br/portal.php"
Given Switch to frame "corpo"
Given fill form entry "l_user" with "200710485"
Given fill form entry "l_pwd" with "123456"
When click on "Entrar" Button
Given Switch to frame "corpo"
Given Switch to frame "conteudoPrincipal"
Given click on tab "abaDoc"
Then I should see the text "No momento não há documentos disponíveis para reimpressão no período permitido."

Scenario: Visualizacao do histórico

Given I am on "https://academicos.unilasalle.edu.br/portal.php"
Given Switch to frame "corpo"
Given fill form entry "l_user" with "200710485"
Given fill form entry "l_pwd" with "123456"
When click on "Entrar" Button
Given Switch to frame "corpo"
Given Switch to frame "conteudoPrincipal"
Given click on tab "abaHistorico"
Then I should see the following table rows:
  | 00205 | Lógica Matemática                         | 4 | 2007/1 | 9,90 |
  | 02462 | Programação Orientada a Objetos           | 4 | 2008/1 | 9,10 |
  | 02463 | Organização e Arquitetura de Computadores | 4 | 2008/1 | 7,70 |
  | 01454 | Teoria da Computação                      | 4 | 2010/1 | 8,00 |
  | 01441 | Inteligência Artificial I                 | 4 |        |      |
  | 01449 | Gerência de Desenvolvimento de Software   | 4 | 2014/1 | 9,30 |