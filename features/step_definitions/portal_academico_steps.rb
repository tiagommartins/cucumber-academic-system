Then(/^I should see user identify "(.*?)"$/) do |text|
	find(:css, 'td.identificaUsuario').should have_content(text)
end

Given(/^Switch to frame "(.*?)"$/) do |iframe_name|
  browser = page.driver.browser
  browser.switch_to.frame(iframe_name)
end

Given(/^click on tab "(.*?)"$/) do |id|
  first(:css, "td##{id}").click
end

Then(/^I should see the following table rows:$/) do |expected_table|
  expected_table.raw.each do |disciplina, g1|
    page.should have_content(disciplina)
    page.should have_content(g1)
  end
end