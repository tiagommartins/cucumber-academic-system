Capybara.app_host = "http://google.com"
Capybara.default_driver = :selenium

Before do
  Capybara.register_driver :selenium_chrome do |app|   
      Capybara::Selenium::Driver.new(app, :browser => :chrome)
  end
  Capybara.current_driver = :selenium_chrome
  Capybara.default_selector = :xpath
end

After do
  #Capybara.use_default_driver
  page.execute_script "window.close();"
  # restart Selenium driver
  Capybara.send(:session_pool).delete_if { |key, value| key =~ /selenium/}
end